module.exports = function( grunt ){
	
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			options:{
				separator:"\n /*** New File ***/ \n"
			},
			/* Added new JS here*/	
			js: { 
				src: [ 
				'./assets/src/js/wrapper/start.js',
				'./node_modules/jquery/dist/jquery.js',
				'./node_modules/bootstrap/dist/js/bootstrap.js',
				'./node_modules/waypoints/lib/jquery.waypoints.js',
				'./node_modules/jquery.counterup/jquery.counterup.js',
				'./node_modules/jquery-inview/jquery.inview.js',
				'./assets/src/js/vendor/easypiechart.js',
				'./node_modules/magnific-popup/dist/jquery.magnific-popup.js',
				'./assets/src/js/vendor/jquery.nav.js',
				'./assets/src/js/main.js',
				'./assets/src/js/wrapper/end.js' 
				],
				dest: './assets/dist/js/script.js'
			},
			/* Add CSS here*/
			css: {
				src: [ 
				'./node_modules/bootstrap/dist/css/bootstrap.css',
				'./node_modules/font-awesome/css/font-awesome.css',
				'./node_modules/magnific-popup/dist/magnific-popup.css',
				'./node_modules/animate.css/animate.css',
				'./assets/src/css/main.css',
				'./assets/src/css/responsive.css'
				],
				dest: './assets/dist/css/main.css'
			}
		},
		uglify: {
			options: {
				report: 'gzip'
			},
			main: {
				src: ['./assets/dist/js/script.js'],
				dest: './assets/dist/js/script.min.js'
			}
		},

		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1,
				keepSpecialComments : 0
			},
			target: {
				files: [{
					expand: true,
					cwd: './assets/dist/css/',
					src: ['*.css', '!*.min.css'],
					dest: './assets/dist/css/',
					ext: '.min.css'
				}]
			}
		},

		copy: {
			main: {
		      files: [
		      { expand: true, cwd: './assets/src/images', src: '**', dest: './assets/dist/images/', filter: 'isFile'},
		      { expand: true, cwd: './assets/src/fonts', src: '**', dest: './assets/dist/fonts/', filter: 'isFile'},
		      { expand: true, flatten: true,  cwd: './node_modules/bootstrap/dist/fonts', src: '**', dest: './assets/dist/fonts/', filter: 'isFile'},
		      { expand: true, flatten: true,  cwd: './node_modules/font-awesome/fonts', src: '**', dest: './assets/dist/fonts/', filter: 'isFile'}
		      ],
		  },
		},
		
		watch: {
			js : {
				files : ['Gruntfile.js','./assets/src/js/*.js'],
				tasks : [ 'js' ]
			},
			css : {
				files : ['./assets/src/*.css'],
				tasks : [ 'css']
			}
		} 
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');
	
    grunt.registerTask( 'css', [ 'concat', 'cssmin'] );
    grunt.registerTask( 'js', [ 'concat', 'uglify' ] );
    grunt.registerTask('default', ['copy', 'concat','uglify','cssmin'] );
}